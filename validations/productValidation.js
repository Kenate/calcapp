const Validator = require('validator');
const isEmpty = require('./isEmpty');

module.exports.validateProductInput = (data) => {
    let errors = {};
    data.name = !isEmpty(data.name) && data.name !== undefined ? data.name : '';
    data.price = !isEmpty(data.price) && data.price !== undefined ? data.price : '';
    data.location = !isEmpty(data.location) && data.location !== undefined ? data.location : '';

    // if (!Validator.isEmpty(data.name)) {
    //     errors.name = 'Name field is required';
    // }
    if (Validator.isEmpty(data.price)) {
        errors.price = 'price field is required';
    }
    if (Validator.isEmpty(data.location)) {
        errors.location = 'location field is required';
    }

    return {
        errors,
        isValid: isEmpty(errors)
    }
}

