const express = require('express');
const Location = require('./../models/location')
var isAuth = require('./../utilities/isAuth');
var { validateProductInput } = require('./../validations/productValidation')

const Router = express.Router();





Router.post('/location', isAuth, async (req, res) => {
    try {
        const data = req.body
        data.createdBy = req.user._id

        const Exists = await Location.findOne({ name: req.body.name, })

        if (Exists) {
            res.status(401).send({ responseCode: "40040", responseMessage: `${req.body.name} is already added` });
        }
        const loc = new Location(data);
        const createdlocation = await loc.save();
        res.status(200).send({ responseCode: "10010", responseMessage: "success", createdlocation });
    } catch (error) {
        console.log(error)
    }
})

Router.get('/location/:id', async (req, res) => {
    if (loc) {
        res.send(loc);
    } else {
        res.status(404).send({ responseCode: "40040", responseMessage: 'Product Not Found' });
    }
})
Router.get('/locations', async (req, res) => {
    const locations = await Location.find();
    if (locations) {
        res.send({ responseCode: "10010", responseMessage: 'locations fetched ', locations });
    } else {
        res.status(404).send({ responseCode: "40040", responseMessage: 'Product Not Found' });
    }
})
    ;


module.exports = Router
