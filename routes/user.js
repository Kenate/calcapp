const express = require('express');
const User = require('./../models/users');
var bcrypt = require('bcryptjs');
var generateToken = require('./../utilities/tokenGen');
var isAuth = require('./../utilities/isAuth');
var { validateRegisterInput } = require('./../validations/userValidation')

const userRouter = express.Router();

userRouter.post('/user/seed', async (req, res) => {
    await User.deleteOne();
    const { errors, isValid } = validateRegisterInput(req.body);
    if (!isValid) {
        return res.status(400).json({ success: true, responseCode: "40040", responseMessage: `Error in your inputs`, errors })

    }
    const body = {
        firstname: req.body.firstname,
        surname: req.body.surname,
        email: req.body.email,
        phone: req.body.phone,
        ranks: req.body.ranks,
        service_No: req.body.service_No,
        password: bcrypt.hashSync(`${req.body.password}`, 8),
        role: 1,
    }

    const createdUsers = await User.insertMany(body);
    return res.status(200).json({ success: true, responseCode: "10010", responseMessage: `User created succeeful`, createdUsers })

})


userRouter.post('/signin', async (req, res) => {
    const user = await User.findOne({ email: req.body.email,deletedAt:null });

    if (user) {
        if (bcrypt.compareSync(req.body.password, user.password)) {
            res.send({
                _id: user._id,
                firstname: user.firstname,
                surname: user.surname,
                role: user.role,
                email: user.email,
                isAdmin: user.isAdmin,
                token: generateToken(user),
            });
            return;
        }
    }
    res.status(400).send({ responseCode: "40040", responseMessage: 'Invalid email or password' });
});

userRouter.post('/register', isAuth, async (req, res) => {
    try {
        const data = req.body
        const userExists = await User.findOne({ email: req.body.email })
        const phoneExists = await User.findOne({ phone: req.body.phone })
        const { errors, isValid } = validateRegisterInput(req.body);
        if (!isValid) {
            res.status(400).send({ responseCode: "40040", responseMessage: `There is error in your input`, errors });
        }
        if (userExists) {
            res.status(400).send({ responseCode: "40040", responseMessage: `${req.body.email} is already used kindly use a different email` });
        }
        if (phoneExists) {
            res.status(400).send({ responseCode: "40040", responseMessage: `${req.body.phone} is already used kindly use a different phone Number` });
        }

        data.password = bcrypt.hashSync(req.body.password, 8)
        const user = new User(data);
        const createdUser = await user.save();
        res.status(200).send({ responseCode: "10010", responseMessage: `${req.body.email} successfullu added `, createdUser });
    } catch (error) {
        console.log(error)
    }
})

userRouter.get('/user/:id', async (req, res) => {
    const user = await User.findById(req.params.id);
    if (user) {
        res.status(200).send({ responseCode: "10010", responseMessage: "User frtched successfully", user });
    } else {
        res.status(404).send({ message: 'User Not Found' });
    }
})
userRouter.get('/user/:id/delete', async (req, res) => {
    await User.findOneAndUpdate({ _id: req.params.id }, { deletedAt: Date() }, { new: true, useFindAndModify: false }, (err, user) => {
        if (err) {
            return res.status(400).json({ success: false,  responseCode: "40040", responseMessage: 'deactivation  failed ', err });
        }

        return res.status(200).json({ success: true,  responseCode: "10010", responseMessage: `${user.surname} Deactivated` });
    })
})
userRouter.get('/users', async (req, res) => {
    const user = await User.find({deletedAt:null});
    if (user) {
        res.send({ responseCode: "10010", responseMessage: "success", user });
    } else {
        res.status(404).send({ responseCode: "40040", responseMessage: 'Users Not Found' });
    }
})
    ;


module.exports = userRouter
