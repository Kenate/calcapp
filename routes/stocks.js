const express = require('express');
var isAuth = require('./../utilities/isAuth');
var moment = require('moment')
var Stock = require('./../models/stocks');
const Product = require('../models/products');
const ProductRouter = express.Router();


ProductRouter.post('/add-stock', isAuth, async (req, res) => {
    try {
        const prod = await Product.findById(req.body.product)

        const data = req.body
        data.location = prod.location
        data.createdBy = req.user._id
        data.date = moment().format('ll')
        newDate = moment().add(1, 'days').format('ll').toString();

        data.opening = req.body.quantity
        const Exists = await Stock.findOne({ product: req.body.product, date: data.date })
        const Exists1 = await Stock.findOne({ product: req.body.product, date: newDate })
        

        if (Exists) {

            const d = { opening: parseInt(Exists.opening) + parseInt(data.quantity) }
            if (Exists.status) {
                const dat = await Stock.findOneAndUpdate({ _id: Exists1._id }, d, { new: true, useFindAndModify: false })
                return res.status(200).json({ success: true, responseCode: "10010", responseMessage: 'added successfull', dat });

            } else {
                const dat = await Stock.findOneAndUpdate({ _id: Exists._id }, d, { new: true, useFindAndModify: false })
                return res.status(200).json({ success: true, responseCode: "10010", responseMessage: 'added successfull', dat });
            }

        } else {

            const newStock = new Stock(data);
            const OpenedStock = await newStock.save();
            return res.status(200).json({ success: true, responseCode: "10010", responseMessage: 'stock added successfull', OpenedStock });
        }

    } catch (error) {
        console.log(error)
        return res.status(400).json({ success: true, responseCode: "40040", responseMessage: `Stock openning failed `, error })

    }
})
ProductRouter.post('/close-stock', isAuth, async (req, res) => {
    try {
        const data = req.body
        newDate = moment().add(1, 'days').format('ll').toString();

        data.createdBy = req.user._id
        data.date = moment().format('ll')
        const OpeningData = {
            opening: req.body.closing,
            daily_sale: 0,
            date: newDate,
            closing: 0,
            product: req.body.product,
            location: req.body.location
        }
        const Exists = await Stock.findOne({ product: req.body.product, date: data.date }).populate('product')
        if(Exists.status === true){
            return res.status(200).json({ success: true, responseCode: "40040", responseMessage: 'You already Closed Today stock' });

        }
        const d = { closing: data.closing, status: true, daily_sale: parseInt(Exists.opening - data.closing) }
        d.total_daily_sales = Exists.product.price * d.daily_sale

        if (Exists) {
            if (Exists.opening < data.closing) {
                return res.status(200).json({ success: true, responseCode: "40040", responseMessage: 'Closing stock should not exceed the opening ' });
            }
            const close = await Stock.findOneAndUpdate({ _id: Exists._id }, d, { new: true, useFindAndModify: false })
            const newStock = new Stock(OpeningData);
            const OpenedStock = await newStock.save();
            return res.status(200).json({ success: true, responseCode: "10010", responseMessage: 'closed successfull', close });
        }

    } catch (error) {
        console.log(error)
    }
})

ProductRouter.get('/stocks', async (req, res) => {
    const data = await Stock.find().populate(["product", "location"]);
    if (data) {
        return res.status(200).json({ success: true, responseCode: "10010", responseMessage: `data fatched succesful`, data })
    } else {
        res.status(404).send({ message: 'stocks Not Found' });
    }
})



module.exports = ProductRouter
