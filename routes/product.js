const express = require('express');
const Product = require('./../models/products');
var isAuth = require('./../utilities/isAuth');
var { validateProductInput } = require('./../validations/productValidation')

const ProductRouter = express.Router();





ProductRouter.post('/product', isAuth, async (req, res) => {
    try {
        const data = req.body
        data.createdBy = req.user._id
        const Exists = await Product.findOne({ name: req.body.name, location: req.body.location })
        const { errors, isValid } = validateProductInput(req.body);
        if (!isValid) {
            return res.status(400).json(errors);
        }
        if (Exists) {
            return res.status(400).send({ responseCode: "40040", responseMessage: `${req.body.name} is already added` });
        }
        const Productd = new Product(data);
        const createdProduct = await Productd.save();
        return res.status(200).send({ responseCode: "10010", createdProduct });
    } catch (error) {
        res.status(400).send({ responseCode: "40040", responseMessage: `Error`, error });

    }
})

ProductRouter.get('/product/:id', async (req, res) => {
    const data = await Product.findById(req.params.id);
    if (data) {
        res.send(data);
    } else {
        res.status(404).send({ responseMessage: 'Product Not Found' });
    }
})
ProductRouter.get('/products', async (req, res) => {
    const Productd = await Product.find().populate('location');
    if (Productd) {
        res.status(200).send(Productd);
    } else {
        res.status(404).send({ responseMessage: 'Products Not Found' });
    }
})
    ;


module.exports = ProductRouter
