const mongoose = require('mongoose');

const stockSchema = new mongoose.Schema(
    {

        opening: { type: Number, default: 0 },
        closing: { type: Number, default: 0 },
        daily_sale: { type: Number, default: 0 },
        status: { type: Boolean, default: false },
        total_daily_sales: { type: Number, default: 0 },
        date: { type: String },
        closing: { type: Number, },
        product: { type: mongoose.Schema.Types.ObjectId, ref: 'Product', },
        location: { type: mongoose.Schema.Types.ObjectId, ref: 'Location', },
        createdBy: { type: mongoose.Schema.Types.ObjectId, ref: 'User', }
    },
    {
        timestamps: true,
    }
);
const Product = mongoose.model('stocks', stockSchema);
module.exports = Product