const mongoose = require('mongoose');

const userSchema = new mongoose.Schema(
    {
        firstname: { type: String },
        surname: { type: String },
        email: { type: String, unique: true },
        phone: { type: String },
        password: { type: String },
        service_No: { type: String, },
        ranks: { type: String, },
        role: { type: String, },
        deletedAt: { type: Date, default: null }
    },
    {
        timestamps: true,
    }
);
const User = mongoose.model('User', userSchema);
module.exports = User