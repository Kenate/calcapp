const mongoose = require('mongoose');

const locationSchema = new mongoose.Schema(
    {
        name: { type: String },
        Location_id: { type: String },
        createdBy :{type: mongoose.Schema.Types.ObjectId, ref: 'User',}
    },
    {
        timestamps: true,
    }
);
const Location = mongoose.model('Location', locationSchema);
module.exports = Location