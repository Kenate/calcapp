const mongoose = require('mongoose');

const productsSchema = new mongoose.Schema(
    {
        name: { type: String },
        location: {type: mongoose.Schema.Types.ObjectId, ref: 'Location',},
        price: { type: Number, },
        createdBy :{type: mongoose.Schema.Types.ObjectId, ref: 'User',}
    },
    {
        timestamps: true,
    }
);
const Product = mongoose.model('Product', productsSchema);
module.exports = Product