var express = require('express')
var cors = require('express')
var dotenv = require('dotenv')
var morgan = require('morgan');
var cors = require('cors');
var bodyParser = require('body-parser')
var mongoose = require('mongoose');
var path = require('path');
const app = express();
const userRoute = require('./routes/user')
const prodRoute = require('./routes/product')
const locationRoute = require('./routes/location')
const stockRoute = require('./routes/stocks')
app.use(morgan('tiny'));
app.use(cors())

require("dotenv").config();

app.use(express.json());

app.use(express.urlencoded({ extended: true }));

mongoose.connect(
    process.env.DB_CONNECT,
    { useNewUrlParser: true, useUnifiedTopology: true },
    () => {
        console.log("Connected to MongoDB");
    }
);
const PORT = process.env.PORT || 9080;


app.use("/api", userRoute);
app.use("/api", prodRoute);
app.use("/api", locationRoute);
app.use("/api", stockRoute);

app.use('/public', express.static(path.join(__dirname, 'public')));

app.use(express.static(path.join(__dirname, 'public')));

app.use(express.static(path.join(__dirname, 'client/build')));


app.get('*', function (req, res) {
    res.sendFile('index.html', { root: path.join(__dirname, 'client/build') });
});

app.listen(PORT, () => {
    console.log(`Serve at http://localhost:${PORT}`);
});